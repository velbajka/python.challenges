#no check of data accuracy included

def invest(amount, rate, years):
    """Tracking the growing amount of an investment over the time """
    for year in range(1, years + 1):
        amount = amount+ (amount * rate) / 100
        print(f"year {year}: ${amount:,.2f}")

amount=float(input("Enter initial amount in $ : "))
rate=float(input("Enter rate in % : "))
years=int(input("Enter number of years : "))

invest(amount, rate, years)
