import random

# # #regularna lista
positions_Player1=[('A',1),('C',1),('E',1), ('G',1), ('B',2), ('D',2),('F',2),('H',2)]
positions_Player2=[('A',7),('C',7),('E',7), ('G',7), ('B',8), ('D',8),('F',8),('H',8)]

# positions_Player1=[('A',1),('C',1),('E',1), ('G',1), ('B',2), ('D',2),('F',2),('H',2)]
# positions_Player2=[('A',7),('C',7),('E',7), ('F',6), ('B',8), ('D',8),('F',8),('H',8), ('D', 4)]

#lista do podwójnego zbijania
# positions_Player1=[('A',1),('C',1),('E',1), ('G',1), ('B',2),('F',2),('H',2)] #('B', 4)
# positions_Player2=[('A',7),('C',7),('E',7), ('G',7), ('B',8), ('D',8),('F',8),('H',8), ('C',3), ('E',5)]

#listy do zbijania podwojnego razem ze zbijaniem do tyłu
# positions_Player1=[('A',1),('C',1),('E',1), ('G',1), ('B',2), ('D',2),('H',2)] #('B', 4)
# positions_Player2=[('A',7),('C',7),('E',7), ('G',7), ('B',8), ('D',8),('F',8),('H',8), ('C',3), ('E',3)]

#listy do checków na komputerze zbijania podwojnego razem ze zbijaniem do tyłu
# positions_Player1=[('A',1),('C',1),('E',1), ('G',1), ('B',2),('H',2)] #('B', 4)
# positions_Player2=[('A',7),('C',7),('E',7), ('G',7), ('B',8), ('D',8),('F',8),('H',8), ('C',3), ('E',3)]

# listy go game over, who won
# positions_Player1=[('E',7), ('B',7)]
# positions_Player2=[('A',3),('E',3)]

u_columns = ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H')
u_rows = (8,7,6,5,4,3,2,1)

board_size = 8

diagonal_row = [-1, -1, 1, 1]
diagonal_column = [1, -1 , -1, 1]

diagonal_2_row = [-2, -2, 2, 2]
diagonal_2_column = [2, -2 , -2, 2]

directions = [(-1,1), (-1, -1), (1, -1), (1, 1)]

round = 0

location_column_to_user = {
    0: 'A',
    1: 'B',
    2: 'C',
    3: 'D',
    4: 'E',
    5: 'F',
    6: 'G',
    7: 'H',
}

location_column = {
    'A': 0,
    'B': 1,
    'C': 2,
    'D': 3,
    'E': 4,
    'F': 5,
    'G': 6,
    'H': 7,
}

location_row = {
    8: 0,
    7: 1,
    6: 2,
    5: 3,
    4: 4,
    3: 5,
    2: 6,
    1: 7,
}
location_row_to_user = {
    0: 8,
    1: 7,
    2: 6,
    3: 5,
    4: 4,
    5: 3,
    6: 2,
    7: 1,
}

class wrongSourceFieldException(Exception):
    pass

class wrongDestinationFieldException(Exception):
    pass

class destinationFieldOccupiedException(Exception):
    pass

class sourceFieldEmptyException(Exception):
    pass

class otherPlayerFigureException(Exception):
    pass

class incorrectDirectionExpection(Exception):
    pass

class emptyMiddleFieldException(Exception):
    pass

class hittingOwnFigureException(Exception):
    pass

class incorrectSingleMovementException(Exception):
    pass

class movementOutOfBoardException(Exception):
    pass

class mandatoryKillingException(Exception):
    pass

class noPossibleMovementFromSelectedSourceException(Exception):
    pass

class Board:
    def __init__(self):
        self.board = []
        for i in range(0, 8):
            self.board.append([' '] * 8)

    def printBoard(self):
        print('   ', end=' ')
        for i in range(8):
            print(location_column_to_user[i], end=' ')
        print()
        k = 8
        z = 9
        for list in self.board:
            print(' ', end='')
            print(k, end=' ')
            k = k - 1
            z = z - 1
            for i in list:
                print('|' + str(i), end='')
            print('| ' + str(z))
        print('   ', end=' ')
        for i in range(8):
            print(location_column_to_user[i], end=' ')
        print()

    def setPlayer1(self, player_1):
        self.player_1 = player_1

    def setPlayer2(self, player_2):
        self.player_2 = player_2

    def cleanBoard(self):
        for i in range(0, 8):
            for j in range(0,8):
                self.board[i][j] = ' '

    def refreshBoard(self):
        self.cleanBoard()
        self.placeFigure(self.player_1)
        self.placeFigure(self.player_2)

    def placeFigure(self, player):
        for figure in player.figures:
            y = location_column[figure[0]]
            x = location_row[figure[1]]
            fig = player.figures[figure]
            color = fig.color
            self.board[x][y] = color

class Figure:
    def __init__(self, column, row, color):
        self.column = column
        self.row = row
        self.color = color

class Man(Figure):
    def __init__(self):
        self.skipLimit = True
        self.moveBackward = False

class Lady(Figure):
    def __init__(self):
        self.skipLimit = False
        self.moveBackward = True

class Player:
    def __init__(self):
        self.figures = []
        self.score = 0
        self.name = 'Player'

    def giveSource(self):
        pass

    def giveDestination(self):
        pass

    def getFigure(self,field_1):
        field=m.translateInputFieldToTuple(field_1)
        for coord_figure in self.figures:
            if coord_figure == field:
                return self.figures[coord_figure]

    def checkIfMandatoryKillingMovementIsPerformed(self, field_1):
        return True

class HumanPlayer(Player):
    def __init__(self):
        self.name = input("Enter your Name: ")
        self.score = 0

    def giveSource(self):
        sourceFieldCorrect = False
        while not sourceFieldCorrect:
            try:
                field_1 = input('Movements start: ')
                m.checkSourceField(field_1)
                sourceFieldCorrect = True
            except wrongSourceFieldException:
                print("Wrong source field")
            except sourceFieldEmptyException:
                print("Source field is empty")
            except otherPlayerFigureException:
                print("Figure belongs to other user")
            except noPossibleMovementFromSelectedSourceException:
                print("No movement is possible from selected source field")
            except mandatoryKillingException:
                print("There is mandatory killing to be performed")
        self.source_field =m.translateInputFieldToTuple(field_1)
        print("Human source field: ", self.source_field)
        return self.source_field

    def giveDestination(self):
        destinationFieldCorrect = False
        while not destinationFieldCorrect:
            try:
                field_2 = input('Movements end: ')
                if not m.checkIfInputFieldHasTwoCharacters(field_2):
                    raise wrongDestinationFieldException()
                destination_field = m.translateInputFieldToTuple(field_2)
                if not m.checkIfDestinationFieldInBoard(field_2):
                    raise wrongDestinationFieldException()
                if m.checkIfSelectedFieldisOccupied(destination_field):
                    raise destinationFieldOccupiedException()
                availableKilling = m.checkIfKillingIsAvailableForPlayer()
                if availableKilling and not self.checkIfMandatoryKillingDestinationIsSelected(field_2):
                    raise mandatoryKillingException()
                if not availableKilling and not self.checkIfCorrectSingleMovementIsPerformed(field_2):
                    raise wrongDestinationFieldException()
                destinationFieldCorrect = True

            except wrongDestinationFieldException:
                print("Wrong destination field")
            except destinationFieldOccupiedException:
                print("Destination field already occupied")
            except mandatoryKillingException:
                print("There is mandatory killing to be performed, check your destination")

        print("Human destination field: ", destination_field)
        return destination_field

    def checkIfCorrectSingleMovementIsPerformed(self, field_2):
        source_y, source_x =  m.translateUserFieldToPython(self.source_field)
        destination_field = m.translateInputFieldToTuple(field_2)
        availableSingleMovements = m.generateAvailableDestinationMovementsForSingleSource(source_y, source_x)
        return destination_field in availableSingleMovements


    def checkIfMandatoryKillingMovementIsPerformed(self, field_1):
        fieldsForKilling = m.generateAvaiableKillingsForPlayerForAllSources()
        source_field = m.translateInputFieldToTuple(field_1)
        return source_field in fieldsForKilling

    def checkIfMandatoryKillingDestinationIsSelected(self, field_2):
        availableKillings = m.generateAvaiableKillingsForPlayerForAllSources()
        destination_field = m.translateInputFieldToTuple(field_2)
        return destination_field in availableKillings[self.source_field]

class ComputerPlayer(Player):
    def __init__(self):
        self.name = 'Computer Player'
        self.score = 0

    def giveSource(self):
        self.sourceFieldCorrect = False
        if m.checkIfKillingIsAvailableForPlayer():
            available_killings = m.generateAvaiableKillingsForPlayerForAllSources()
            self.source_field = random.choice(list(available_killings.keys()))
            self.sourceFieldCorrect = True
            print("Computer source field: ", self.source_field)
            return self.source_field

        while not self.sourceFieldCorrect:
            self.source_field = random.choice(list(self.figures.keys()))
            if m.checkIfMovementPossibleFromSource(self.source_field):
                self.sourceFieldCorrect = True
                print("Computer source field: ", self.source_field)
                return self.source_field

    def giveDestination(self):
        y, x = m.translateInputFieldToPython(self.source_field)
        destinationFieldCorrect = False
        if m.checkIfKillingIsAvailableForPlayer():
            available_killings = m.generateAvaiableKillingsForPlayerForAllSources()
            print("available killings", available_killings)
            destination_field = random.choice(available_killings[self.source_field])
            print("Computer destination field: ", destination_field)
            return destination_field
        while not destinationFieldCorrect:
            try:
                field = self.generateCoordinates(y, x)
                destination_field = m.translateFieldToUser(field)
                if m.checkIfSelectedFieldisOccupied(destination_field):
                    raise destinationFieldOccupiedException()
                destinationFieldCorrect = True
                print("Computer destination field: ", destination_field)
                return destination_field
            except:
                destinationFieldOccupiedException
                print("Selected destination field already occupied, another one is chosen", destination_field)

    def generateCoordinates(self, y, x):
        if self.color == 'o':
            x -= 1
        else:
            x += 1
        if y > 0 and y < 7:
            y += random.choice([-1, 1])
        elif y == 0:
            y += 1
        else: # y == 7
            y -= 1
        return y,x

    def checkIfMandatoryKillingMovementIsPerformed(self, field_1):
        return True

    def generateKillingMovement(self):
        fieldsForKilling = m.generateAvaiableKillingsForPlayerForAllSources()
        return random.choice(fieldsForKilling)


class GameManager:
    def __init__(self):
        self.current_player = None
        self.second_player = None
        self.createPlayers()
        self.setPlayersOrder()
        self.createFigures()
        self.createBoard()
        self.setPlayersOnBoard()

    def setPlayersOrder(self):
        start_player = random.randint(1,2)
        if start_player == 1:
            self.player_1.color = 'o'
            self.player_2.color = 'x'
            print(f"{self.player_1.name} starts")
            self.current_player = self.player_1
            self.second_player = self.player_2
        else:
            self.player_1.color = 'x'
            self.player_2.color = 'o'
            print("Computer starts")
            self.current_player = self.player_2
            self.second_player = self.player_1

    def checkIfInputFieldHasTwoCharacters(self, field):
        return len(field) == 2

    def splitInputFieldToColumnAndRow(self, field):
        column_user = (field[0]).upper()
        row_user = int(field[1])
        return column_user, row_user

    def translateInputFieldToTuple(self, field):
        column_user=(field[0]).upper()
        row_user=int(field[1])
        source=(column_user, row_user)
        return source

    def translateInputFieldToPython(self, field):
        column = location_column[(field[0]).upper()]
        row = location_row[int(field[1])]
        return column, row

    def translateFieldToUser(self, field):
        column, row = field
        column_to_user = location_column_to_user[column]
        row_to_user = location_row_to_user[row]
        return column_to_user, row_to_user

    def translateUserFieldToPython(self, field):
        column, row = field
        column_to_python = location_column[column]
        row_to_python = location_row[row]
        return column_to_python, row_to_python

    def translateCoordinatesToUser(self, y, x):
        column_to_user = location_column_to_user[y]
        row_to_user = location_row_to_user[x]
        return column_to_user, row_to_user

    def checkIfUserFiedlInBoard(self, field):
        u_col, u_row = field
        return u_col in u_columns and u_row in u_rows

    def checkIfSourceFieldInBoard(self, field_1):
        column1, row1 = self.splitInputFieldToColumnAndRow(field_1)
        return column1 in location_column and row1 in location_row

    def checkIfDestinationFieldInBoard(self, field_2):
        column2, row2 = self.splitInputFieldToColumnAndRow(field_2)
        return column2 in location_column and row2 in location_row

    def checkIfSourceFieldBelongsToOtherPlayer(self, field_1):
        source = self.translateInputFieldToTuple(field_1)
        return source in self.second_player.figures

    def startGame(self):
        self.board.refreshBoard()
        self.board.printBoard()
        while not self.checkIfGameisOver():
            keyField1 = self.current_player.giveSource()
            keyField2 = self.current_player.giveDestination()
            # self.performMovement(keyField1, keyField2)
            if self.checkIfKillingIsAvailableForPlayer():
                self.performMovement(keyField1, keyField2)
                self.removeFigureAfterKillingFromParticularField(keyField1, keyField2)
                self.board.refreshBoard()
                self.board.printBoard()
                while self.checkIfAnotherKillingIsAvailableFromDestinationField(keyField2):
                    self.current_player.source_field = keyField2
                    print(f"{self.current_player.name} new source field", self.current_player.source_field)
                    destination = self.current_player.giveDestination()
                    self.performMovement(self.current_player.source_field, destination)
                    self.removeFigureAfterKillingFromParticularField(self.current_player.source_field, destination)
                    self.board.refreshBoard()
                    self.board.printBoard()
                    keyField2 = destination
                    print("New destination: ", keyField2)
            else:
                self.performMovement(keyField1, keyField2)
                self.board.refreshBoard()
                self.board.printBoard()

            if self.checkIfFinalFieldIsReached(keyField2):
                self.addScoreForFinalFieldAndTakeAwayFigure(keyField2)
                self.board.refreshBoard()
                self.board.printBoard()

            print(f'{self.current_player.name} score', self.current_player.score)
            # self.board.refreshBoard()
            # self.board.printBoard()
            self.switchPlayers()
        winner = self.whoHasBiggerScore()
        print(f"And the winner is {winner.name}")

    def performMovement(self, keyField1, keyField2):
        col = self.getFigureColor(keyField1)
        self.current_player.figures.pop(keyField1, None)
        self.current_player.figures[keyField2] = Figure(keyField2[0], keyField2[1], col)

    def checkIfAnotherKillingIsAvailableFromDestinationField(self, destination_field):
        # print("destination as source", destination_field)
        y,x = self.translateUserFieldToPython(destination_field)
        return self.generateAvailableKillingDestinationForSingleSource(y, x) != []

    def checkIfKillingIsAvailableForSingleField(self, y, x):
        return self.generateAvailableKillingDestinationForSingleSource(y, x) != []

    def checkIfFinalFieldIsReached(self, destination_field):
        destination_field_python = self.translateUserFieldToPython(destination_field)
        destination_y, destination_x = destination_field_python
        if self.current_player.color == 'o':
            if destination_x == 0:
                return True
        else:
            if destination_x == 7:
                return True
        return False

    def addScoreForFinalFieldAndTakeAwayFigure(self, destination_field): #tupla jak dla usera
        self.current_player.figures.pop(destination_field, None)
        self.current_player.score += 1

    def whoHasBiggerScore(self):
        if self.current_player.score > self.second_player.score:
            return self.current_player
        else:
            return self.second_player

    def removeFigureAfterKillingFromParticularField(self, source_field, destination_field):
        middle_field = self.generateMiddleFieldForSourceAndDestination(source_field, destination_field)
        self.second_player.figures.pop(middle_field, None)

    def generateMiddleFieldForSourceAndDestination(self, source_field, destination_field):
        source_y, source_x = self.translateUserFieldToPython(source_field)
        destination_y, destination_x = self.translateUserFieldToPython(destination_field)
        middle_field_y = (source_y + destination_y)//2
        middle_field_x = (source_x + destination_x)//2
        middle_field_python = middle_field_y, middle_field_x
        middle_field = m.translateFieldToUser(middle_field_python)
        return middle_field

    def checkIfGameisOver(self):
        return self.checkIfNoFiguresLeft()

    def checkIfNoFiguresLeft(self):
        return self.current_player.figures == {} or self.second_player.figures == {}

    def getFigureColor(self, field):
        color=self.current_player.figures[field].color
        return color

    def switchPlayers(self):
        self.current_player, self.second_player=self.second_player, self.current_player

    def createPlayers(self):
        self.player_1 = HumanPlayer()
        self.player_2 = ComputerPlayer()

    def createFigures(self):
        figures_1 = {}
        figures_2 = {}
        for f in positions_Player1:
            figures_1[f] = Figure(f[0], f[1], 'o')
        for f in positions_Player2:
            figures_2[f] = Figure(f[0], f[1], 'x')
        #przypisanie figur do gracza
        if self.current_player == self.player_1:
            self.player_1.figures = figures_1
            self.player_2.figures = figures_2
        else:
            self.player_1.figures = figures_2
            self.player_2.figures = figures_1

    def createBoard(self):
        self.board = Board()

    def printBoard(self):
        self.board.printBard()

    def setPlayersOnBoard(self):
        self.board.setPlayer1(self.player_1)
        self.board.setPlayer2(self.player_2)

    def checkIfSelectedFieldisOccupied(self, field_2):
        column2_user, row2_user = self.splitInputFieldToColumnAndRow(field_2)
        return (column2_user, row2_user) in self.current_player.figures or (column2_user, row2_user) in self.second_player.figures

    def checkIfMovementPossibleFromSource(self, field_1):
        y, x = self.translateInputFieldToPython(field_1)
        regular_movements_exist = self.generateAvailableDestinationMovementsForSingleSource(y, x) != []
        killing_movements_exist = self.generateAvailableKillingDestinationForSingleSource(y, x)!= []
        return regular_movements_exist or killing_movements_exist

    def generateWhiteMovementLeft(self, y, x):
        x -= 1
        if y > 0 and y <= 7:
            y -= 1
        else:
            raise movementOutOfBoardException()
        return (y, x)

    def generateWhiteMovementRight(self, y, x):
        x -= 1
        if y >= 0 and y < 7:
            y += 1
        else:
            raise movementOutOfBoardException()
        return (y, x)

    def generateBlackMovementLeft(self, y, x):
        x += 1
        if y > 0 and y <= 7:
            y -= 1
        else:
            raise movementOutOfBoardException()
        return (y, x)

    def generateBlackMovementRight(self, y, x):
        x += 1
        if y >= 0 and y < 7:
            y += 1
        else:
            raise movementOutOfBoardException()
        return (y, x)

    def generateAvaiableMovementsForPlayerForAllSources(self):
        allSingleMovementsMap = {}
        for fig in m.current_player.figures:
            y, x = m.translateUserFieldToPython(fig)
            fig_destination_movements = self.generateAvailableDestinationMovementsForSingleSource(y, x)
            if fig_destination_movements != []:
                destination_movements = self.translateListOfTuplesToUser(fig_destination_movements)
                allSingleMovementsMap[fig] = destination_movements
        print(allSingleMovementsMap)
        return allSingleMovementsMap

    def generateAvailableDestinationMovementsForSingleSource(self, y, x):
        availableDestinationMovements=[]
        if self.current_player.color == 'o':
            try:
                field1 = self.generateWhiteMovementLeft(y, x)
                field_left = self.translateFieldToUser(field1)
                left_field_free = field_left not in self.current_player.figures and field_left not in self.second_player.figures
            except movementOutOfBoardException:
                left_field_free = False
            try:
                field2 = self.generateWhiteMovementRight(y, x)
                field_right = self.translateFieldToUser(field2)
                right_field_free = field_right not in self.current_player.figures and field_right not in self.second_player.figures
            except movementOutOfBoardException:
                right_field_free = False

        else:
            try:
                field1 = self.generateBlackMovementLeft(y, x)
                field_left = self.translateFieldToUser(field1)
                left_field_free = field_left not in self.current_player.figures and field_left not in self.second_player.figures

            except movementOutOfBoardException:
                left_field_free = False
            try:
                field2 = self.generateBlackMovementRight(y, x)
                field_right = self.translateFieldToUser(field2)

                right_field_free = field_right not in self.current_player.figures and field_right not in self.second_player.figures
            except movementOutOfBoardException:
                right_field_free = False

        if left_field_free:
            availableDestinationMovements.append(field_left)
        if right_field_free:
            availableDestinationMovements.append(field_right)
        return availableDestinationMovements

    def generateKillingCoordinates(self, y, x):
        coordinates_for_killing_fields=[]
        for i in range(4):
            new_y = y + diagonal_2_column[i]
            new_x = x +diagonal_2_row[i]
            coordinates_for_killing_fields.append((new_y, new_x))
        return coordinates_for_killing_fields

    def generateKMiddleCoordinates(self, y, x):
        corrdinates_of_middle_fields=[]
        for j in range(4):
            new_y = y + diagonal_column[j]
            new_x = x + diagonal_row[j]
            corrdinates_of_middle_fields.append((new_y, new_x))
        return corrdinates_of_middle_fields

    def checkIfFieldBelongsToOtherPlayer(self, y, x):
        checked_field = m.translateCoordinatesToUser(y, x)
        return checked_field in self.second_player.figures

    def checkIfKillingIsAvailableForSingleField(self, y, x):
        return self.generateAvailableKillingDestinationForSingleSource(y, x) != []

    def checkIfKillingIsAvailableForPlayer(self):
        return self.generateAvaiableKillingsForPlayerForAllSources() != {}

    def generateAvaiableKillingsForPlayerForAllSources(self):
        allKillings={}
        for fig in m.current_player.figures:
            y, x = m.translateUserFieldToPython(fig)
            fig_killings = self.generateAvailableKillingDestinationForSingleSource(y, x)
            if fig_killings != []:
                destination_killings = self.translateListOfTuplesToUser(fig_killings)
                allKillings[fig]= destination_killings
        return allKillings

    def translateListOfTuplesToUser(self, python_list):
        new_user_list = []
        for element in python_list:
            y , x = element
            new_element = self.translateCoordinatesToUser(y, x)
            new_user_list.append(new_element)
        return new_user_list

    def generateAvailableKillingDestinationForSingleSource(self, y, x):
        fieldsForKilling=[]
        killingCoordinates = self.generateKillingCoordinates(y, x)
        for kill_coor in killingCoordinates:
            ind = killingCoordinates.index(kill_coor)
            kill_y, kill_x = kill_coor
            if self.checkIfFieldInBoard(kill_y, kill_x) and not self.checkIfKillingPythonFieldisOccupied(kill_y, kill_x):
                corrdinates_of_middle_fields = self.generateKMiddleCoordinates(y, x)
                middle_field = corrdinates_of_middle_fields[ind]
                mid_y, mid_x = middle_field
                if self.checkIfFieldBelongsToOtherPlayer(mid_y, mid_x):
                    fieldsForKilling.append((kill_y, kill_x))
        return fieldsForKilling

    def checkSourceField(self, field_1):
        if not self.checkIfInputFieldHasTwoCharacters(field_1):
            raise wrongSourceFieldException()
        if not self.checkIfSourceFieldInBoard(field_1):
            raise wrongSourceFieldException()
        if not self.checkIfSelectedFieldisOccupied(field_1):
            raise sourceFieldEmptyException()
        if self.checkIfSourceFieldBelongsToOtherPlayer(field_1):
            raise otherPlayerFigureException()
        if not self.checkIfMovementPossibleFromSource(field_1):
            raise noPossibleMovementFromSelectedSourceException()
        if self.checkIfKillingIsAvailableForPlayer() and not self.current_player.checkIfMandatoryKillingMovementIsPerformed(field_1):
            raise mandatoryKillingException()

    def checkIfFieldInBoard(self, y, x):
        return (y in range(0, board_size) and x in range(0, board_size))

    def checkIfKillingPythonFieldisOccupied(self, y, x):
        user_column, user_row = m.translateFieldToUser((y, x))
        return ((user_column, user_row) in self.current_player.figures) or ((user_column, user_row) in self.second_player.figures)


m=GameManager()
m.startGame()


