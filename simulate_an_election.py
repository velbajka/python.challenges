import random

number_of_elections=10_000
A_probability_of_winning_region_1 = 0.87
A_probability_of_winning_region_2 = 0.65
A_probability_of_winning_region_3 = 0.17

def A_winning_probability(A_probability_of_winning):
    if random.random() < A_probability_of_winning:
        return 1
    else:
        return 0

def calculate_election_with_A_winning():
    """ Calculates probability of Candidate A winning depending on number of elections """

    A_winning_count=0

    for i in range(number_of_elections):

        region_1=A_winning_probability(A_probability_of_winning_region_1)

        region_2=A_winning_probability(A_probability_of_winning_region_2)

        region_3=A_winning_probability(A_probability_of_winning_region_3)

        sum=region_1+region_2+region_3

        if sum>=2:
            A_winning_count = A_winning_count+1

    probability=A_winning_count/number_of_elections*100

    return probability

print(f"Probability of Cadidate A becoming a mayor: {calculate_election_with_A_winning()}% ")
