class Person:
    def __init__(self, name, last_name, age, sex):
        self.name=name
        self.last_name=last_name
        self.age=age
        self.sex=sex

class Teacher(Person):
    def __init__(self, subject):
        self.subject=subject
        self.reservations=[]

    def addReservation(self, reservation):
        pass

    def deleteReservation(self, reservation):
        pass

    def printReservationForTeacher(self):
        pass

class Student(Person):
    def __init__(self, class_level_number):
        self.class_level_number=class_level_number

    def printStudent(self):
        pass

class ClassNumber:
    def __init__(self):
        self.class_students=[]

    def addStudentToAClass(self, student):
        pass

    def deleteStudentFromAClass(self, student):
        pass

    def printStudentsInClass(self):
        pass

class Building:
    def __init__(self, number_of_floors):
        self.number_of_floors=number_of_floors
        self.floors=[]

    def getRoom(self, floor_number, room_number):
        pass

class Floor:
    def __init__(self, floor_number):
        self.floor_number=floor_number
        self.rooms=[]

    def getRoom(self, room_number):
        pass

class Room:
    def __init__(self, room_number, floor_number):
        self.room_number=room_number
        self.floor_number=floor_number

    def printRoom(self):
        pass

class Reservation:
    def __init__(self, reservation_date, reservation_hour, duration, room_number, class_level_number):
        self.reservation_date=reservation_date
        self.reservation_hour=reservation_hour
        self.duration=duration
        self.room_number=room_number
        self.class_level_number=class_level_number

    def printReservation(self):
        pass

class SchoolManager:
    def __init__(self):
        self.teachers=[]
        self.classes=[]

    def addTeacher(self, teacher):
        pass

    def deleteTeacher(self, teacher):
        pass

    def addClass(self, classNumber):
        pass

    def deleteClass(self, classNumber):
        pass

    def checkRoomAvailability(self, reservation_date, reservation_hour, duration, floor_number, room_number):
        pass

    def reserveARoom(self, reservation_date, reservation_hour, duration, room_number, class_level_number):
        pass

    def printReservationForTeachers(self):
        pass

    def findAProperRoom(self, subject):
        pass

    def printStudentsInClasses(self):
        pass
