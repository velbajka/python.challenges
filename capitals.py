import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
    }


state_list = []
for key in capitals_dict.keys():
    state_list.append(key)
    state = random.choice(state_list)

correct_answer=False

while correct_answer==False :

    capital=input(f"Capital of state {state} is: ")
    if capital.upper() ==capitals_dict[state].upper():
        correct_answer = True
        print("Your answer is correct!")
    elif capital.upper()=='EXIT':
        correct_answer = True
        print(f"Correct answer is {capitals_dict[state]}. Goodbye!")
