import random
import os


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')

ROZMIAR_SWIATA = 20
GÓRA = 'w'
LEWO = 'a'
PRAWO = 'd'
DÓŁ = 's'


class ŚwiatGry:

    def __init__(self):
        self.bohaterowie = []
        self.rozmiarSwiata = ROZMIAR_SWIATA
        self.czyKoniecGry = False

    def setGracz(self, gracz):
        self.gracz = gracz

    def printKoniecGry(self):
        if self.gracz.czyWygrany:
            print("Koniec gry - wygrałeś.")
        else:
            print("Koniec gry - przegrałeś.")

    def dodajBohatera(self, bohater):
        self.bohaterowie.append(bohater)

    def usuńBohatera(self, bohater):
        self.bohaterowie.remove(bohater)

    def generujWarstweGry(self):
        warstwa = [' '] * ROZMIAR_SWIATA
        warstwa.append('|')
        warstwa.insert(0, '|')
        return warstwa

    def generujSwiatGry(self):
        self.niebo = self.generujWarstweGry()
        self.swiat = self.generujWarstweGry()

        if self.gracz.czyWyskoczyl:
            self.niebo[self.gracz.położenie] = self.gracz.wyglad
        else:
            self.swiat[self.gracz.położenie] = self.gracz.wyglad

        for b in self.bohaterowie:
            self.swiat[b.położenie] = b.wyglad

    def wyswietlNiebo(self):
        print('\n' * 78)
        print("Licznik:", self.gracz.licznikPunktów)
        print('\n' * 1)
        for i in self.niebo:
            print(i, end='')
        print()

    def wyswietlSwiatGry(self):
        self.wyswietlNiebo()
        for i in self.swiat:
            print(i, end='')

    def ktoPodeMnaLubNaMnie(self):
        for b in self.bohaterowie:
            if self.gracz.położenie == b.położenie:
                return b
        return None

    def czyKtosPodeMnaLubNaMnie(self):
        return self.ktoPodeMnaLubNaMnie()!= None

class Bohater:
    def __init__(self, wyglad, czyMożeSięPoruszać, położenie):
        self.wyglad = wyglad
        self.czyMożeSięPoruszać = czyMożeSięPoruszać
        self.położenie = położenie

class Moneta(Bohater):
    def __init__(self, wyglad, czyMożeSięPoruszać, położenie, wartosc):
        super(Moneta, self).__init__(wyglad, czyMożeSięPoruszać, położenie)
        self.wartosc = wartosc

class Gracz(Bohater):
    def __init__(self, wyglad, czyMożeSięPoruszać, położenie):
        super(Gracz, self).__init__(wyglad, czyMożeSięPoruszać, położenie)
        self.czyWyskoczyl = False
        self.iloscRuchowWNiebie = 0
        self.licznikPunktów = 0


    def umrzyj(self):
        świat.czyKoniecGry = True
        self.czyWygrany = False

    def zwiekszLicznik(self, wartosc):
        self.licznikPunktów = self.licznikPunktów + wartosc

    def setŚwiatGry(self, światGry):
        self.światGry = światGry

    def obsłużKolizje(self):
        if świat.czyKtosPodeMnaLubNaMnie():
            bohater = świat.ktoPodeMnaLubNaMnie()
            if not self.czyWyskoczyl and isinstance(bohater, Moneta):
                self.zwiekszLicznik(bohater.wartosc)  # moneta
                świat.usuńBohatera(bohater)
            elif not self.czyWyskoczyl:
                self.umrzyj()  # roslinka

    def obsłużRuchLewo(self):
        if self.położenie > 1:
            self.położenie = self.położenie - 1
            self.obsłużKolizje()
            if self.czyWyskoczyl:
                if self.iloscRuchowWNiebie == 0:
                    self.iloscRuchowWNiebie = 1
                else:
                    self.czyWyskoczyl = False
                    self.iloscRuchowWNiebie = 0
                    self.obsłużKolizje()

        elif self.położenie == 1:
            if self.czyWyskoczyl:
                self.iloscRuchowWNiebie = 1
                self.czyWyskoczyl = False

    def obsłużRuchPrawo(self):
        if self.położenie == świat.rozmiarSwiata:
            świat.czyKoniecGry = True
            self.czyWygrany = True
            return

        self.położenie = self.położenie + 1

        self.obsłużKolizje()

        if self.czyWyskoczyl:
            if self.iloscRuchowWNiebie == 0:
                self.iloscRuchowWNiebie = 1
            else:
                self.czyWyskoczyl = False
                self.iloscRuchowWNiebie = 0
                self.obsłużKolizje()


    def obsłużRuchDół(self):
        if self.czyWyskoczyl:
            self.iloscRuchowWNiebie = 0
            self.czyWyskoczyl = False
            czyKtosPodeMna = self.światGry.czyKtosPodeMnaLubNaMnie()
            bohaterPodeMna = self.światGry.ktoPodeMnaLubNaMnie()
            if czyKtosPodeMna and isinstance(bohaterPodeMna, Moneta):
                self.zwiekszLicznik(bohaterPodeMna.wartosc)  # moneta
                świat.usuńBohatera(bohaterPodeMna)
            elif czyKtosPodeMna:
                self.licznikPunktów += 1
                self.światGry.usuńBohatera(bohaterPodeMna)

    def przechwycRuch(self, rodzajRuchu):
        if rodzajRuchu == PRAWO:
            self.obsłużRuchPrawo()
        elif rodzajRuchu == LEWO:
            self.obsłużRuchLewo()
        elif rodzajRuchu == GÓRA:
            if self.czyWyskoczyl == True:
                self.czyWyskoczyl = False
            else:
                self.czyWyskoczyl = True
        elif rodzajRuchu == DÓŁ:
            self.obsłużRuchDół()

mario = Gracz(wyglad='M', czyMożeSięPoruszać=True, położenie=1)
roslinka = Bohater(wyglad='R', czyMożeSięPoruszać=False, położenie=7)
moneta = Moneta(wyglad='P', czyMożeSięPoruszać=False, położenie=12, wartosc=5)
świat = ŚwiatGry()
świat.setGracz(gracz=mario)
mario.setŚwiatGry(świat)

świat.dodajBohatera(bohater=roslinka)
świat.dodajBohatera(bohater=moneta)


świat.generujSwiatGry()
świat.wyswietlSwiatGry()

while not świat.czyKoniecGry:
    ruch = input()
    mario.przechwycRuch(ruch)
    świat.generujSwiatGry()
    świat.wyswietlSwiatGry()

świat.printKoniecGry()
