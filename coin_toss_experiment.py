import random

def average_flips_per_trial():

    """ Number of flips for the sequence containing both heads and tails """

    heads_tally=0
    tails_tally=0
    number_of_flips=0
    number_of_trials=0

    while number_of_trials <= 10_000:

        coin_flip=random.randint(0, 1)

        if coin_flip == 0:
            heads_tally = heads_tally + 1
        else:
            tails_tally = tails_tally + 1

        number_of_flips = number_of_flips + 1

        if heads_tally!=0 and tails_tally!=0:
            number_of_trials = number_of_trials + 1
            heads_tally = 0
            tails_tally = 0

    average=number_of_flips/number_of_trials

    return average

print(average_flips_per_trial())
