cats={}

for i in range(1,11):
    cats[i]=False

print(cats)

for round in range(1,4):
    for cat, hat in cats.items():
        if cat%round==0:
            if cats[cat]==True:
                cats[cat] = False
            else:
                cats[cat]= True

for cat, hat in cats.items():
    if cats[cat]==True:
        print(f"Cat number {cat} wears a hat")
    else:
        print(f"Cat number {cat} doesn't wear a hat")
