def set_password():
    password = input("Tell me your password:")
    while len(password) < 1:
        password = input("Tell me your password again:")

    return password.upper()

def create_password_list(password):
    password_list=[]
    for letter in password:
        if letter == ' ':
            password_list.append(' ')
        else:
            password_list.append('_')

    return password_list

def guess_a_letter():
    letter_correct=False
    while letter_correct==False:
        move = input("Give a letter: ")
        if len(move)==1:
            letter_correct = True

    return move.upper()

def check_successful_guess(move, password):
    if move in password:
        successfull_guess=True
    else:
        successfull_guess = False

    return successfull_guess

def adjust_display_password(password, display_password, move):
    for i in range(len(password)):
        if move==password[i]:
            display_password[i] = move

    return  display_password

def show_password_status(display_password):
    for letter in display_password:
        print(letter, end='')
    print()

def print_game_status(fail):
    if fail==1:
        print()
        print()
        print()
        print()
        print("|")
        print("|")
        print("|")
        print()
    if fail == 2:
        print()
        print("|")
        print("|")
        print("|")
        print("|")
        print("|")
        print("|")
        print()
    if fail == 3:
        print()
        print("________________")
        print("|")
        print("|")
        print("|")
        print("|")
        print("|")
        print("|")
        print("|")
        print()
    if fail == 4:
        print()
        print("_________________")
        print("|               |")
        print("|")
        print("|")
        print("|")
        print("|")
        print("|")
        print("|")
        print()
    if fail == 5:
        print()
        print("_________________")
        print("|               |")
        print("|               @")
        print("|")
        print("|")
        print("|")
        print("|")
        print("|")
        print()
    if fail == 6:
        print()
        print("_________________")
        print("|               |")
        print("|               @")
        print("|               |")
        print("|               |")
        print("|")
        print("|")
        print("|")
        print()
    if fail == 7:
        print()
        print("_________________")
        print("|               |")
        print("|               @")
        print("|              /|\\")
        print("|               |")
        print("|              / \\")
        print("|")
        print("|")
        print()



###################GAME START############################################
print("LET'S START A HANGMAN GAME")
print()

fail=0
password=set_password()
# print(password)
display_password=create_password_list(password)

winner=False
show_password_status(display_password)
while fail<7 and winner==False:
    move=guess_a_letter()

    if check_successful_guess(move, password):
        adjust_display_password(password, display_password, move)
        print(f"Good guess, {move} is in the password.")
        if not '_' in display_password:
            winner=True
    else:
        print(f"Bad shot, {move} is not in the password.")
        fail=fail+1

    print()
    show_password_status(display_password)
    print_game_status(fail)

if fail==7:
    print("Game over, you've lost!")

if winner==True:
    print("Huray, you know the whole password!")


