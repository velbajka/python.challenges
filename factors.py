import math

def find_all_factors(n):
    """ Finds all factors of a given number """
    i = 1
    factors = []
    while i <= math.sqrt(n):
        if n % i == 0:
            factors.append(i)
            factors.append(n//i)
        i = i + 1

    factors = sorted(set(factors))

    return factors

def print_all_factors(n):
    list_of_factors = find_all_factors(n)
    for i in range(0, len(list_of_factors)):
        print(f"{list_of_factors[i]} is a factor of {n}")


n = int(input("Enter a positive integer: "))
print_all_factors(n)
