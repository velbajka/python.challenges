#in case end_page bigger than the last page in pdf document, no error
#in case end_page<start_page, exit

import easygui as gui
from PyPDF2 import PdfFileReader, PdfFileWriter

start_page_value_correct=False
end_page_value_correct=False

input_path = gui.fileopenbox(
    title="Select a PDF file",
    default="*.pdf"
)
if input_path is None:
    exit()

start_page = gui.enterbox(msg = "Start page", title = "Give your first page")

if start_page is None:
    exit()

while start_page_value_correct==False:
    try:
        start_page = int(start_page)
        start_page_value_correct = True
    except ValueError:
        gui.msgbox(msg="This is not a number")
        start_page = gui.enterbox(msg="Start page", title="Give your first page")
    if start_page is None:
        exit()

while start_page<=0:
    gui.msgbox(msg="This number is incorrect")
    start_page = gui.enterbox(msg="Start page", title="Give your first page")
    if start_page is None:
        exit()

end_page = gui.enterbox(msg = "End page", title = "Give your last page")

if end_page is None:
    exit()

while end_page_value_correct==False:
    if start_page is None:
        exit()
    try:
        end_page = int(end_page)
        end_page_value_correct = True
    except ValueError:
        gui.msgbox(msg="This is not a number")
        start_page = gui.enterbox(msg="End page", title="Give your last page")
    if start_page is None:
        exit()

while end_page<=0:
    gui.msgbox(msg="This number is incorrect")
    end_page = gui.enterbox(msg="End page", title="Give your last page")
    if start_page is None:
        exit()

if end_page<start_page:
    exit()

save_title = "Save extracted pages as..."
file_type = "*.pdf"
output_path = gui.filesavebox(title=save_title, default=file_type)

if output_path is None:
    exit()
while input_path == output_path:
    gui.msgbox(msg="Cannot overwrite original file!")
    output_path = gui.filesavebox(title=save_title, default=file_type)
    if output_path is None:
        exit()

input_pdf = PdfFileReader(input_path, strict=False)
output_pdf = PdfFileWriter()

for page in input_pdf.pages[start_page:(end_page+1)]:
    output_pdf.addPage(page)

with open(output_path, mode="wb") as output_file:
    output_pdf.write(output_file)


