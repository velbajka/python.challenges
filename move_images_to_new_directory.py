from pathlib import Path

document_path=Path.home()/"Practice_Files"/"documents"
document_path.mkdir(parents=True, exist_ok=True)

for i in range(1,6):
    file = document_path / f"file{i}.txt"
    file.touch()

for i in range(1,5):
    image = document_path / f"image{i}.png"
    image.touch()

practice_path=images_path=Path.home()/"Practice_Files"
images_path=practice_path/"images"
images_path.mkdir(exist_ok=True)

for path in document_path.glob("*.png"):
    source=path
    image=path.name
    destination=images_path/image
    source.replace(destination)

