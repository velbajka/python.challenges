import csv
from pathlib import Path

scores = [
    {"name": "LLCoolDave", "score": 23},
    {"name": "LLCoolDave", "score": 27},
    {"name": "red", "score": 12},
    {"name": "LLCoolDave", "score": 26},
    {"name": "tom123", "score": 26},
    {"name": "Misha46", "score": 21},
    {"name": "Misha46", "score": 25},
    {"name": "Empiro", "score": 23},
    {"name": "Empiro", "score": 10},
    ]

path=Path.home()/"scores.csv"
file=path.open(mode="w", encoding="utf-8",newline='')
writer=csv.DictWriter(file, fieldnames=scores[0].keys())
writer.writeheader()
writer.writerows(scores)
file.close()
scores=[]

def modify_row(row):
    row["score"] = int(row["score"])
    return row

file_path = Path.home() / "scores.csv"
with file_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.DictReader(file)
    for row in reader:
        scores.append(modify_row(row))


print(scores)
high_scores={}


def find_highest_score():
    for row in scores:
        name=row["name"]
        score=int(row["score"])
        if name not in high_scores:
            high_scores[name]=score
        else:
            if high_scores[name]<score:
                high_scores[name] = score

    return high_scores


print(find_highest_score())


new_file_path = Path.home() / "high_scores.csv"
with new_file_path.open(mode="w", encoding="utf-8", newline='') as new_file:
    writer=csv.DictWriter(new_file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for name in high_scores:
        row={"name":name, "high_score":high_scores[name]}
        writer.writerow(row)



