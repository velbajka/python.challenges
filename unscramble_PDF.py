from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

def get_page_text(page):
    return page.extractText()


pdf_path = Path.home()/"scrambled.pdf"
pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()

pages = {}

for page in pdf_reader.pages:
    text=page.extractText()
    number=int(text[0])

    if page["/Rotate"]!=0:
        page.rotateCounterClockwise(page["/Rotate"])

    pages[number]=page

for key in sorted(pages):
	pdf_writer.addPage(pages[key])

output_path=Path.home()/"unscrambled.pdf"
with output_path.open(mode="wb") as output_file:
	pdf_writer.write(output_file)


