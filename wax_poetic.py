import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives =["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon","for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]


random_nouns=[]
random_verbs=[]
random_adj=[]
random_prep=[]
random_adv=[]

for i in range(3):
    noun = random.choice(nouns)
    random_nouns.append(noun)

    verb = random.choice(verbs)
    random_verbs.append(verb)

    adjective=random.choice(adjectives)
    random_adj.append(adjective)

for i in range(2):
    preposition = random.choice(prepositions)
    random_prep.append(preposition)

adverb=random.choice(adverbs)
random_adv.append(adverb)

vowels= ["a", "e", "i", "o", "u"]

if random_adj[0][0] in vowels:
    first_prep="An"
else:
    first_prep = "A"

if random_adj[2][0] in vowels:
    middle_prep = "an"
else:
    middle_prep = "a"

print(first_prep, random_adj[0],random_nouns[0])
print(first_prep, random_adj[0],random_nouns[0], random_verbs[0], random_prep[0], "the" , random_adj[1] , random_nouns[1])
print(random_adv[0], "the" , random_nouns[0], random_verbs[1])
print("the", random_nouns[1], random_verbs[2], random_prep[1], middle_prep ,random_adj[2], random_nouns[2])
