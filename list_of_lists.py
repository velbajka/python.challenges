
universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

def enrollment_stats(universities):
    enrollments=[]
    fees=[]
    for i in range(len(universities)):
        enrollments.append(universities[i][1])
        fees.append(universities[i][2])
    return enrollments, fees


def mean(my_list):

    my_list_mean=sum(my_list)/len(my_list)

    return my_list_mean

def median(my_list):

    my_list.sort()

    lists_length=len(my_list)
    if lists_length%2!=0:
        median_index = lists_length // 2
        my_list_median=my_list[median_index]

    else:
        median_index1 = lists_length // 2
        median_index2 = lists_length // 2 - 1
        my_list_median = (my_list[median_index1] + my_list[median_index2]) / 2

    return my_list_median



print(enrollment_stats(universities))

enrollments, fees = enrollment_stats(universities)
total_number_of_students=sum(enrollments)
total_tuition=sum(fees)
mean_total_number_of_students=mean(enrollments)
median_total_number_of_students=median(enrollments)
mean_tuition=mean(fees)
medain_tuition=median(fees)

print(f"Total number of students: {total_number_of_students}")
print(f"Total tuition: {total_tuition} $")
print(f"Student mean: {mean_total_number_of_students:.2f}")
print(f"Student median: {median_total_number_of_students:.2f}")
print(f"Tuition mean: {mean_tuition:.2f} $")
print(f"Tuition median: {medain_tuition:.2f} $")




