from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter


class PdfFileSplitter:
    def __init__(self, file_path):
        self.file_path = file_path


    def split(self, breakpoint):

        pdf=PdfFileReader(str(self.file_path))

        number_of_pages=pdf.getNumPages()

        first_part=pdf.pages[0:breakpoint]
        second_part=pdf.pages[breakpoint:number_of_pages]

        self.pdf_writer1=PdfFileWriter()
        self.pdf_writer2=PdfFileWriter()


        for page in first_part:
            self.pdf_writer1.addPage(page)

        for page in second_part:
            self.pdf_writer2.addPage(page)

    def write(self, filename):

        output_path1=Path.home()/(filename+"_1.pdf")
        output_path2=Path.home()/(filename+"_2.pdf")

        with output_path1.open(mode='wb') as output_file1:
            self.pdf_writer1.write(output_file1)

        with output_path2.open(mode='wb') as output_file2:
            self.pdf_writer2.write(output_file2)


pdfSplitter=PdfFileSplitter(file_path=Path.home() / "Python Basics.pdf")
pdfSplitter.split(breakpoint=150)
pdfSplitter.write(filename="mydoc_split")
