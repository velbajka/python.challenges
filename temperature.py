def convert_cel_to_far(C):
    """Celcius degrees represented in Fahrenheit degrees """
    F = C * 9/5 + 32
    print(f"{C} degrees C = {F:,.2f} degrees F")

def convert_far_to_cel(F):
    """Fahrenheit degrees  represented in Celcius degrees """
    C = (F - 32) * 5 / 9
    print(f"{F} degrees F = {C:,.2f} degrees C")

F_value_correct=False
C_value_correct=False

while F_value_correct==False:
    F=input("Enter a temperature in degrees F: ")
    try:
        F=float(F)
        convert_far_to_cel(F)
        F_value_correct=True
    except ValueError:
        print("This is not a number")

while C_value_correct==False:
    C=input("Enter a temperature in degrees C: ")
    try:
        C=float(C)
        convert_cel_to_far(C)
        C_value_correct=True
    except ValueError:
        print("This is not a number")
