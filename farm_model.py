class Animal:

    def __init__(self, name, age, colour, weight, sex):
        self.name=name
        self.age=age
        self.colour=colour
        self.weight=weight
        self.sex=sex

    def talk(self, sound):
        if sound == None:
            return "Hello"
        else:
            return f"{self.name} says {sound}"


    def do(self, activity):
        if activity==None:
            return f"{self.name} has a rest day"
        else:
            return f"{self.name} is {activity}"

class Cow(Animal):

    def talk(self, activity="mu"):
        return super().talk(activity)

    def do(self, activity="chewing"):
        return super().do(activity)

    def give_milk(self, amount):
        return f"{self.name} gives {amount} l of milk"

class Pig(Animal):

    def talk(self, activity="chrum"):
        return super().talk(activity)

    def do(self, activity="taking mud-bath"):
        return super().do(activity)

class Chicken(Animal):

    def talk(self, activity="koko"):
        return super().talk(activity)

    def do(self, activity="pecking"):
        return super().do(activity)

    def give_eggs(self, number):
        if number==0:
            return "This is a boy, you will find no eggs"
        else:
            return f"{self.name} gives {number} eggs"

mućka=Cow(name="Mućka", age=10, colour = "brown", weight= 700, sex="female")
peppa=Pig(name="Peppa", age=2, colour = "ping", weight= 250, sex="female")
sheldon=Chicken(name="Sheldon", age=1, colour = "black", weight= 0.85, sex="male")

print(mućka.talk())
print(mućka.do())
print(mućka.give_milk(amount=10))
print()
print(peppa.talk())
print(peppa.do())
print()
print(sheldon.talk())
print(sheldon.do())
print(sheldon.give_eggs(number=0))
print()

animal=Animal(name="Some animal", age="age", colour="colour", weight="weight", sex="sex")

print(animal.talk())
print(animal.do())
